---
extends: _layouts.post
title: Artisans Welcome!
author: Steve Popoola
github: https://github.com/stevepop
date: 2017-06-05
tag: General
section: body
---

Laravel UK is the community I wish I had come across when I just started out as a developer and
most especially when I started trying out Laravel. In spite of my years of experience, I am
happy to be a part of this community which started off during the last Laracon Online Conference
which held on March 8th 2017.
                    
If you are experienced or you are just starting out your development career or you just fancy
hanging out with cool and helpful bunch of smart people, Laravel UK is it.
Check us out on <a href="http://twitter.com/UKLaravel" target="_blank">Twitter</a> or join our <a href="https://laraveluk.signup.team" target="_blank">Slack Group</a></p>