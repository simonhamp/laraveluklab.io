---
extends: _layouts.post
title: Welcome To Laravel UK
author: Symfonycoda
github: https://github.com/symfonycoda
date: 2017-06-06
tag: General
section: body
---

Welcome to the new LaravelUK website folks. Due to the huge growth of the Slack group, it seemed like we needed to evolve and expand, so with the help of some prominent community members that joined in the early days, our new baby has been born. Special thanks to the amazing <a href="https://github.com/stevepop">@stevepop</a> for his work here, without him you would not be reading this.


We have designed the site to be expanded as we go, but initially it is a place where we can all store any resources that we think our community members may find useful, since they are deleted from the #resources Slack channel after what seems like no time at all! In addition, you are all welcome to write your own blog posts, or link to posts you have written elsewhere, or even add a post about a product or service you have created (please use this sensibly though!).


For those who may not know the background of the Community, LaravelUK was started at the beginning of Feb 2017, with the hope that we could have 20-30 like-minded UK devs, who could network, discuss all things tech, and crucially be able to provide help and support to each other in real time. The members grew to around 30 at the time of the LaraconLive, and a few well placed ads in the chatrooms encouraged many more users to join our cause. This was followed up by a Twitter campaign of inviting UK users to join, and today, at the time of writing 4 months on, we are only a couple of members short of 200!


As you know, the Laravel community in general has a really good, inclusive feel to it, and our LaravelUK group has added the niche aspect of all being from the same geographic location, and we have already started to promote future local meetups in the Midlands. I have already seen friendships that have started by people chatting in the Slack channels, and in fact, nearly 80% of messages sent now are Direct Messages, further implying that friendships are happening away from the public channels.


Thank you so much to everybody who has helped to make this community the wonderful place that it is. From spreading the word about the group, to being active in discussions, and helping others out in the #helpneeded channel. We have conference speakers, business owners, full-time devs, entrepreneurs, and learners all with a common interest and enjoying each others company.

If you are reading this post, and you haven't joined the community, this is what to do:

<a href="https://laraveluk.signup.team" target="_blank">Slack Invite</a>

<a href="http://twitter.com/UKLaravel" target="_blank">Oficial Twitter</a>


I'm really proud to have started this ride, and I hope you will all join me as we expand in the future.


Thanks guys
SymfonyCoda