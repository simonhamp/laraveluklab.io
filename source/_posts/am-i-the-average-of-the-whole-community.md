---
extends: _layouts.post
title: Am I The Average Of The Whole Community?
author: symfonycoda
date: 2017-09-19
tag: LaravelUK
section: body
---

## AM I THE AVERAGE OF THE WHOLE COMMUNITY?


**“You are the average of the five people you spend the most time with.” – Jim Rohn**



I remember reading the above quote several years ago from Jom Rohn, a motivational speaker and self-help guru. 

I remember relating it to other people rather than myself. I had a friend at the time who I had known since our school days, who always managed to be in situations where trouble seemed to find him, and I recall that the people that he spent the most time with, often found themselves in similar situations. 

I also remember a documentary about some people who challenged each other to push themselves in their worklife and learning, and held meetings where they met solely to report back to the group on what they had achieved, and to set new goals for the time period until the next meeting. Unsurprisingly, the were all extremely high achievers. 

For some reason however, I didn't relate it to my own life at the time. Yet, more recently, as I look back over certain time periods, I can see how the company I kept has at times meant stagnation or has greatly encouraged me to achieve, in other words affecting what I accomplished during those years. So, although this post will not name names or go into detail about what occurred, I hope it will give a sense of what the quote now means to me, and how it relates to this wonderful LaravelUK community that I am so proud of having started.

So join me now as I ramble through a few thoughts related to this topic, and lets see where we end up


### MY NON-EXISTANT PROGRAMMING BACKGROUND

For those of you who don't know, I made a big decision in 2016, to learn PHP programming, and look to change career into web development, something completely different from the promotion and marketing and sales work that I had previously done. I began with no experience whatsoever, apart from knowing some basic HTML and CSS, and soon I discovered a framework which seemed like the way forward called Symfony. 

It was at this point where I came across another very valuable life lesson, "Don't run before you have learned to walk". In this case, it meant don't try tackling a hugely complex PHP framework, before learning PHP, OOP, Git, Composer, Namespacing, Autoloading, and the many many other things that I now take for granted. Quite simply, the learning curve was just far too steep for me at that point in time, and for a couple of months I gave up completely


### FINDING LARAVEL

Then, almost exactly a year ago, I decided to give it another go, I started looking at some PHP basics, and I came across some Youtube videos including one by Codecourse, and got intrigued by the concept of Laravel. It seemed like it had a much lower entry point than Symfony, but was still extremely powerful. Next I found some Laracasts videos and the more I watched, the more I got hooked. I could also see that the community and learning ecosystem were in place for me to get started quickly. 

Then several things happened at the same time: 

- My day job was being relocated so that I could walk to work in 10 minutes, instead of having an hour commute in each direction

- Laravel 5.3 was about to be released

- I split up with my girlfriend 

Okay, so here we had the perfect storm of additional spare time (10 hours a week), a shiny new version of Laravel, and a need to get my mind stuck into a new challenge - "Lets Do This" was my thinking.

For those first few weeks, I vowed to spend 2 hours every day learning and improving my code skills. I subscribed to Laracasts and Codecourse, I watched videos, I set myself challenges, I read documentation, I broke stuff and rebuilt it. I went back and learned from the ground up with Standalone PHP, OOP and Git, and all the other things that were needed to be able to create using Laravel. And I was doing ok, in fact I was learning for several hours most days, instead of the original 2 hours I had as an aim!

Over the next few months, I was also looking for something that would give me just that little bit extra, a community where I could get to know other Laravel users, where I could get help in return for being able to advise others on the marketing / branding / promotional side of what they were doing. I couldn't find anything that quite fitted what I wanted. 



### SETTING UP LARAVELUK

Fast forward to the end of January 2017, and I still hadn't found what I was looking for. I searched for a few people online and asked if they would join, and the response was positive, so in February 2017 I started the LaravelUK Slack community, and began the task of promoting it to enourage other users to join. 

Some of my earlier years in promotions were in nightclubs, and i remembered times when I had brought new people into a club night, who then became friends or partners, and in one case a couple got married! I wanted to be able to bring strangers together and allow friendships to begin here as well 

Fast forward again 8 months, and we have well over 350 members, with many of them regular contributors to the discussions, help, and banter that the community has come to be known for. It really is a great place to be, not only because I have had realtime help, but also because I have met some great people



### BACK TO THE ORIGINAL QUESTION

Going back to the original question though, if you are the average of the five people you spend most time with, how has having a community full of go-getting developers changed me and my outlook? The answer is "Dramatically". It has changed my whole mindset and attitude to a lot of things.

Firstly, I never knew what was possible in terms of freelancing, the money that can be earned and the flexibility it can give you. We started out having a freelance channel for discussion, and later two of our members have set up the [Coding Solo Podcast](https://codingsolo.works/) which addresses all things freelance. If you haven't heard it, you definitely need to listen to @davzie and @alexbilbie on there. 

Secondly, the motivation that some of the guys have, to work so many hours to complete their projects on time, and then to work more hours on  their side projects, and additional learning is incredible. And then I look at myself, I started with a thought of learning for 10 hours per week, but now most weeks I code (or spend time on the LaravelUK community) for probably as many hours as I do in my day job. And just recently, a year on from beginning this journey, I have started down the road of my first freelancing gig!

My developing skills have definitely been influenced by the other members of the community in a very positive way. There is no way I could have learned what I have learned so quickly without the help of the other members, and I am eternally grateful to everyone who has given their time (and in some cases their patience) to give help to myself and other members.

Also, when I started to spend a vast amount of time chatting with developers, it made me feel like I was a developer. And as my own knowledge increased, the feeling of Imposter Syndrome very quickly went away, and I now class myself as a developer. It feels like the biggest influence on my personality has been my peer group, and in this case the LaravelUK community, and I know I'm not the only one in there that feels this way: 

**@iamdanbarrett - "I have found my love for development again being in this group. I started to feel isolated from all the other devs doing great things out there. Even considered doing something totally different"**

**@rick - "has massively helped my motivation after starting going solo late last year. No devs to talk to in the real world"**

Comments like these are further inspiring, and make me feel that all the hard work setting up the community has been worth it, and I know that it is not only me that has felt the wave of positivity throughout the community. Remote devs and freelancers can feel part of a team, and everybody feels part of something special.


### CONCLUSIONS 

As to whether I am the average of the whole community, it is very difficult to answer, but I do know that I am glad I set up LaravelUK, and I know that the influence of the people I chat to most often in the community has changed me for the better.




If you haven't joined LaravelUK yet, please request an invite at [laraveluk.signup.team](http://laraveluk.signup.team) and join our [official twitter](http://twitter.com/UKLaravel)





