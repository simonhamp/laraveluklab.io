---
extends: _layouts.post
title: Laravel London - September 2017
author: simonhamp
github: https://github.com/simonhamp
date: 2017-09-09
tag: Laravel
section: body
---

On Tuesday evening, the largest throng of UK Artisans yet to meet at Laravel London came together at the dizzying heights of Energy Aspects' office on the 32nd Floor of 25 Canada Square, Canary Wharf.

It's been quite a while since the last Laravel London and it seemed like everyone was chomping at the bit to get involved: it was a sell-out! It was great to see many familiar faces along with a bunch of new ones.

The Talks
-------------
First up was *Glenn Jacobs* (sadly not WWE's Kane) who braved London in an ankle boot to speak to us about his team's new open source e-commerce project, [GetCandy](https://getcandy.io/).

Glenn's talk was spirited and enjoyable and he did a great job of presenting the reasoning behind GetCandy and teasing us with an early preview into what the system will be able to do.

It looks really interesting. There's nothing really out there at the moment for more generalised e-commerce in Laravel, and GetCandy looks like it's taking the right approach on a lot of things. I'm personally very keen to get involved and try it out.

It's also great to know that it's got the backing of a successful agency - it really feels like it could be a serious contender as a good open source e-commerce solution, but even more so for us Laravel lovers.

Thanks, Glenn. Hope that leg gets better.

Next up, *Steve Popoola* brought his humour and charm to a relatively new but powerful feature of Laravel: Notifications.

Steve showed us first-hand just how easy it is to use Notifications and the `Notifiable` trait, along with some practical examples of what he uses them for.

Notifications are a really useful way of centralising and grouping together methods of notifying your users when certain events occur within your application. One of the main take-aways from Steve's talk was just how simple Laravel makes all of this - not to be taken for granted!

Thanks, Steve!

Well that about wraps it up. Big thanks to both our speakers for sharing their projects and learning with us. Also huge props to Amo Chohan and Energy Aspects for sponsoring the venue and drinks. And of course, massive thanks to Jonty Behr for organising, MC'ing and organising the delicious (and massive) pizzas!