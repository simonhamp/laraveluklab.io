---
extends: _layouts.post
title: Laravel on Bash on Ubuntu on Windows!
author: Rich Peers
date: 2017-06-12
tag: Laravel
section: body
---
Before we were introduced to "Bash on Ubuntu on Windows" - the Windows Subsystem for Linux, Windows was still a viable 
Operating System for PHP development. Using combinations of Git Bash, Homestead, Vagrant, Docker and other tools to 
circumvent the fact it's not Unix like. Essential tools like Composer and NodeJS offer Windows native installs, which when used via Git Bash can work quite well; most of the time. When something does'nt work, be prepared to spend serious time finding a workaround.

You can choose to use a Virtual Machine like Vagrant, Homestead, or Docker. Mirror the server environment as close as possible, and avoid messing up your local machine. Use a VM and you can scrap it then spin another up in minutes. Thus making the host O.S. seemingly irrelevant. However, you might want to run CLI tools locally on the shared folder. Running `npm run dev` every time you "Just move that up one pixel" can really slow you down if ran on a V.M.

For the Linux Subsystem with the "Creative Update"; Apache, nginx, MySQL, PostgreSQL and more now work out the box. Pulling in dependencies and compiling assets is quicker than on a VM, but slower than native Windows. Its a matter of balance, and preference, which tools to use via the Linux Subsystem "Bash on Ubuntu on Windows", in native Windows, or via a Virtual Machine.

With Virtual machines for close to production environments, and Valet for quickly serving a `new laravel app` locally, I'm now using the Linux Subsystem for php, composer, npm and git commands from `/mnt/c/code`. I use a Windows branch of Valet parked on `c:\code` which i can share with a VM if fit for the project. MySQL I run natively, although it would work just as well inside the subsystem and accessed at localhost from either.

## Configuring Windows Subsystem for Linux
I recommend the [MSDN Installation Guide](https://msdn.microsoft.com/en-gb/commandline/wsl/install_guide).

Guidance for configuring PHP, Composer, Git NodeJS and Yarn below. As you can see it's all standard stuff for Ubuntu. It all works!

### PHP
```bash
sudo apt-get update | sudo apt-get upgrade -y
```

**7.0** ships with Ubuntu 16.04:
```bash
sudo apt-get install php php-mysql php-mbstring php-xml php-curl php-zip
```

For other versions use [ondrej/php](https://launchpad.net/~ondrej/+archive/ubuntu/php/):
```bash
sudo apt-get install python-software-properties -y
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update | sudo apt-get upgrade -y
```

**7.1**:
```bash
sudo apt-get install php7.0 php7.0-mysql php7.0-mbstring php7.0-xml php7.0-curl php7.0-zip
```

**5.6**:
```bash
sudo apt-get install php5.6 php5.6-mysql php5.6-mbstring php5.6-xml php5.6-curl php5.6-zip
```

### Composer
DigitalOcean have good instructions to [Install and Use Composer on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-16-04)

### Git
```bash
sudo apt-get git
```
Configure Identity: [First-Time Git Setup](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

### NodeJS and npm
Official instruction to [Install Node.js](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) 

### Yarn
Installation - [Debian/Ubuntu Linux](https://yarnpkg.com/en/docs/install#linux-tab)

## Laravel Mix & Browsersync
Consider the following in `webpack.mix.js` to allow browsersync to open chrome when you `npm run watch` 


```
if (mix.inProduction()) {
    mix.disableNotifications()
        .version();
} else {
    mix.browserSync({
        proxy: 'your-domain.dev', // Valet
        // proxy: 'localhost:8000', // php artisan serve
        // proxy: 'your-domain.app', // Homestead
        browser: '/mnt/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe' // windows linux subsystem
    });
}
```
