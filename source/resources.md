---
extends: _layouts.master
section: content
---

# Resources

The following useful links are resources shared by our community members.

 * [Laravel](https://laravel.com) The PHP Framework For Web Artisans
 * [Mailgun](https://www.mailgun.com) The Email Service For Developers
 * [Mailtrap](https://mailtrap.io) - A fake SMTP server for development teams to test, view and share emails sent from the development and staging environments without spamming real customers.
 * [Collect](https://github.com/tightenco/collect) - A Collections-only split from Laravel's Illuminate Support
 * [Bueify](https://buefy.github.io/#/) - Lightweight library of responsive UI components for Vue.js based on Bulma 
 * [Laravel Media Library](https://github.com/spatie/laravel-medialibrary) - for syntax highlighting in output code blocks
 * [Lighthouse](https://developers.google.com/web/tools/lighthouse) - Lighthouse is an open-source, automated tool for improving the quality of web pages
 * [Laravel Sketchpad](https://github.com/davestewart/laravel-sketchpad) - An innovative front-end environment for interactive Laravel development
 * [MarkdownMail](https://markdownmail.com/themes) - A Web App to Create Laravel Markdown Email Themes
 * [Laravel Snappy](https://github.com/barryvdh/laravel-snappy) - This package is a ServiceProvider for [Snappy](https://github.com/KnpLabs/snappy)
 * [Doorman](https://laravel-news.com/doorman)-  Limit access to your Laravel applications through invite codes
 * [Favro](https://www.favro.com/) - A planning and collaboration app teams and managers love.
 