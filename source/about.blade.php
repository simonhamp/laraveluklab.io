@extends('_layouts.master')

@section('content')

        <article class="entry">
            <header class="entry-header">

                <h2 class="entry-title intro-section">
                    About Laravel UK
                </h2>

            </header>

            <div class="entry-content">
                <p>Laravel UK is a community of Laravel developers which started off during the last Laracon Online Conference
                    which held on March 8th 2017. Although the group was initially set up  to provide meeting point for Laravel
                    developers and enthusiasts in the United Kingdom, we also have developers from other countries like India, Pakistan
                    and Netherlands.</p>
                <p>If you are experienced or just starting out your development career or you just fancy
                    hanging out with a cool and helpful bunch of smart people, Laravel UK is it.</p>
                <p>Check us out on <a href="http://twitter.com/UKLaravel" target="_blank">Twitter</a> or join our <a
                            href="https://laraveluk.signup.team/" target="_blank">Slack Group</a></p>
            </div>

        </article>
        <!-- end entry -->
@endsection
