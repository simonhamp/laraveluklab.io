@extends('_layouts.master')

@section('content')

    @foreach ($intros as $intro)
        <article class="entry">
            <header class="entry-header">

                <h2 class="entry-title">
                    <a href="{{ $intro->getPath() }}" title="">{{ $intro->title }}</a>
                </h2>

                <div class="entry-meta">
                    <ul>
                        by <li><a href="{{ $intro->github }}">{{ $intro->author }}</a></li>
                    </ul>
                </div>

            </header>

            <p class="entry-content">
                {!! $intro->excerpt() !!}...<a href="{{ $intro->getPath() }}"> <span class="read-more"> Read more </span></a>
            </p>

        </article>
    @endforeach
    <!-- end intro -->
        <hr>
    <!-- start posts collection -->
    @foreach ($posts as $post)
        <article class="entry">
            <header class="entry-header">

                <h2 class="entry-title">
                    <a href="{{ $post->getPath() }}" title="">{{ $post->title }}</a>
                </h2>

                <div class="entry-meta">
                    <ul>
                        <li>{{ date('d/m/Y', $post->date) }}</li>
                        <span class="meta-sep">&bull;</span>

                        <li><a href="#" title="" rel="category tag">{{ $post->tag }}</a></li>
                        <span class="meta-sep">&bull;</span>

                        <li>{{ $post->author }}</li>
                    </ul>
                </div>

            </header>

            <p class="entry-content">
                {!! $post->excerpt() !!}...<a href="{{ $post->getPath() }}"> <span class="read-more"> Read more </span></a>
            </p>

        </article> <!-- end entry -->
        <hr>
    @endforeach

    <!-- end posts collection -->
@endsection
