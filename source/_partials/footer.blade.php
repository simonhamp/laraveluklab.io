   <div class="row">
      <div class="twelve columns">
      <ul class="social-links">
             <li><a href="https://twitter.com/UKLaravel"><i class="fa fa-twitter"></i></a></li>
             <li><a href="https://github.com/laraveluk"><i class="fa fa-github-square"></i></a></li>
             <li><a href="https://laraveluk.signup.team"><i class="fa fa-slack"></i></a></li>

          </ul>
      </div>

       <div class="eight columns info">

          <h3>About Laravel UK</h3>

          <p>We are a community of developers across UK and beyond. Our aim is to provide an environment where Laravel
          Artisans can provide support for each other and have fun building awesome stuff together
          </p>

       </div>

       <div class="four columns">
          <h3 class="social">Navigate</h3>

          <ul class="navigate group">
             <li><a href="{{ $page->baseUrl }}/">Home</a></li>
             <li><a href="{{ $page->baseUrl }}/about">Who We Are</a></li>
             <li><a href="{{ $page->baseUrl }}/archives">Archives</a></li>
             <li><a href="{{ $page->baseUrl }}/resources">Resources</a></li>
          </ul>
       </div>

       <p class="copyright">&copy; Copyright 2017 Laravel UK.</p>

    </div> <!-- End row -->

    <div id="go-top"><a class="smoothscroll" title="Back to Top" href="#top"><i class="fa fa-chevron-up"></i></a></div>
