@extends('_layouts.master')

@section('content')
    <h1>{{ $page->title }}</h1>
    <p>By {{ $page->author }}</p>

    @yield('body')
@endsection