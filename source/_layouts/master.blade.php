<!DOCTYPE html>
<head>

  <meta charset="utf-8">
  <title>Laravel UK - A Great Laravel Community in the UK</title>
  <meta name="description" content="LaravelUK A Community of Laravel Artisans within the UK and beyond">
  <meta name="author" content="Laravel UK">
  <meta name="keywords" content="laravel, php, web, development, artisans, coding">
  <meta name="apple-mobile-web-app-title" content="LaravelUK"/>

  <!-- mobile specific metas
   ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <!-- CSS
    ================================================== -->
   <link rel="stylesheet" href="{{ $page->baseUrl }}/css/main.css">

   <!-- Favicon
  ================================================== -->
  <link rel="shortcut icon" href="{{ $page->baseUrl }}/favicon.ico" >
</head>

<body>

     <!-- Header
     ================================================== -->
     <header id="top">

        @include('_partials.header')

     </header> <!-- Header End -->

     <!-- Content
     ================================================== -->
     <div id="content-wrap">

         <div class="row">
             <div id="main" class="eight columns">
                @yield('content')
             </div>

             <!-- Sidebar -->
             <div id="sidebar" class="four columns">
                 @include('_partials.sidebar')
             </div>
            <!-- end sidebar -->

         </div>

     </div> <!-- end content-wrap -->



     <!-- Footer
     ================================================== -->
     <footer>

        @include('_partials.footer')

    </footer> <!-- End Footer-->

  </body>

</html>
