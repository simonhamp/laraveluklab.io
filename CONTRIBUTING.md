## Laravel UK Blog

This project contains the source for the LaravelUK blog https://laraveluk.gitlab.io/.

## Motivation
The motivation behind this project is to provide a resource for Laravel UK community members to contribute writings, news, ideas and helpful hints for the benefit of the community as well as visitors to the website


## Installation

- Clone the repository - `git clone git@gitlab.com:laraveluk/laraveluk.gitlab.io.git`
- Run `composer install`
- Set up your webserver to serve the project directory


## Contribution
If you wish to contribute to this project, you will be required to do the following;
- Clone/Fork the project
- Create a Post. All posts collections are in the `source/_posts` folder. See other posts as sample of how to create yours
- After you finish adding your post, compile the html by running, `./vendor/bin/jigsaw build production`
- Commit your post and create a merge request
- Once your request is accepted and merged, you should see the post on the website

