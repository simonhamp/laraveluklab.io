<?php

return [
    'baseUrl' => '',
    'production' => false,
    'sitename' => 'Laravel UK',
    'collections' => [
        'posts' => [
            'excerpt' => function ($page, $characters = 300) {
                return substr(strip_tags($page->getContent()), 0, $characters);
            },
            'sort' => '-date',
        ],
        'intros' => [
            'excerpt' => function ($page, $characters = 680) {
                return substr($page->getContent(), 0, $characters);
            },
            'sort' => '-date',
        ]
    ],

    'selected' => function ($page, $section) {
        return str_contains($page->getPath(), $section) ? 'current' : '';
    },
];
